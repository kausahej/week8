/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week8;

/**
 *
 * @author sahej
 */
public class Simulation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String password = "Sahej";
		String password1 = "ABC!@#";
		String password2 = "ONEtWOT!";
		String password3 = "!@#%$^";
		
		method(password);
		method(password1);
		method(password2);
		method(password3);
	}

    /**
     *
     * @param password
     */
    public static void method(String password) {
		PasswordValidator validator = new PasswordValidator();
		if(validator.validate(password)) {
			System.out.println("The" + password + " is a strong password.");
		}
                else {
			System.out.println("The" + password + " is a weak password.");
		}
    }
    
}
