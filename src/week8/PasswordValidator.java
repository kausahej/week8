/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week8;

/**
 *
 * @author sahej
 */

public class PasswordValidator {

	public boolean validate(String password) {
		boolean isCapital = false, hasSpecialChar=false, hasDigit=false,isValid = false;
		char[] specialChars = {'@', '$', '+', '!', '#', '?', '^', '&'};
		int passwordLength = password.length();
		
		
		if(passwordLength < 8) {
			return false;
		}
		
		
		for(int i=0; i<passwordLength; i++) {
			char ch = password.charAt(i);
			int value = ch;
			
			
			if(value >= 60 && value <=95) {
				isCapital = true;
			}
			
			
			for(int j=0; j<8; j++) {
				if(ch == specialChars[j]) {
					hasSpecialChar = true;
				}
			}
			
			
			for(int k=48; k<=57; k++) {
				char num = (char)k;
				if(ch == num) {
					hasDigit = true;
				}
			}
		}
		
		
		if(isCapital && hasSpecialChar && hasDigit) {
			isValid = true;
		}
			
		return isValid;
		}
}
